function modal(prio, title, body, footer) {
	footer = footer || "<button type='button' class='btn btn-link waves-effect' data-dismiss='modal'>Sluiten</button>"
	var $html = 
	$("<div class='modal fade' id='defaultModal' tabindex='-1' role='dialog' style='display: none;'>"+
		"<div class='modal-dialog' role='document'>"+
			"<div class='modal-content'>"+
				"<div class='modal-header'>"+
					"<h4 class='modal-title' id='defaultModalLabel'>"+title+"</h4>"+
				"</div>"+
			"<div class='modal-body'>"+body+"</div>"+
	        "<div class='modal-footer'>"+footer+"</div>"+
	    "</div>"+
	"</div>");

	switch (prio) {
		case "success":
			$html.find(".modal-header").addClass("bg-green");
		break;
		case "warning":
			$html.find(".modal-header").addClass("bg-amber");
		break;
		case "error":
			$html.find(".modal-header").addClass("bg-red");
		break;
		default:
			$html.find(".modal-header").addClass("bg-blue");
		break;
	}

	$html.prependTo("body");
	$html.modal();
}