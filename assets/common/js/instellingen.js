var instellingen = [];

$(function() {
	getInstellingen();
	$(document).on("keyup", "#wachtwoord", function() {
		if($("#wachtwoord").val() == "") {
			$("#wachtwoord2Wrapper").slideUp(200);
			$("#wachtwoord2").val("");
		} else {
			if($("#wachtwoord2Wrapper").is(":hidden")) {
				$("#wachtwoord2Wrapper").slideDown(200);
			}
		}
	});


	$(document).on("submit", "#formInstellingen", function(e) {
		e.preventDefault();
		var formData = $(this).serializeArray();
		$.ajax({
		    url: '/backsite/instellingen.php',
		    type: 'PUT',
		    data: formData,
		    success: function(result) {
		    	if(result.success == true) {
		    		getInstellingen();
		    		modal("success", "Instellingen opgeslagen", "De instellingen zijn opgeslagen");
		    		$("#wachtwoord").val("");
					$("#wachtwoord2Wrapper").slideUp(200);
					$("#wachtwoord2").val("");
		    	} else {
		    		var error = result.error || "Er is iets mis gegaan!";
		    		modal("error", "Fout!", error);
		    	}
		    }
		});
	});

	$(document).on("click", "#newAPIkeyForm", function() {
		modal("warning", 
			"Let op!", 
			"Weet je zeker dat je de API key wilt wijzigen? Als de de API key veranderd moet je deze ook opnieuw instellen bij diensten zoals google assistant.",
			"<button type='button' class='btn btn-link waves-effect' data-dismiss='modal'>Sluiten</button><button type='button' id='newAPIkeyModal' class='btn btn-primary waves-effect' data-dismiss='modal'>Wijzigen</button>");
	});
	$(document).on("click", "#newAPIkeyModal", function() {
		$("#APIkey").val(generateUUID);
	})
});

function getInstellingen() {
	$.get("backsite/instellingen.php", function(data) {
		instellingen = data;
		setStandaardwaarden();
	});
}

function setStandaardwaarden() {
	$("#leftsidebar .user-info .image").text(instelling("naam").substring(1,0));
	$("#leftsidebar .user-info .name").text(instelling("naam"));
	$("#leftsidebar .user-info .email").text(instelling("email"));

	if($("#formInstellingen").length > 0) {
		$("#formInstellingen #naam").val(instelling("naam"));
		$("#formInstellingen #email").val(instelling("email"));
		$("#formInstellingen #APIkey").val(instelling("APIkey"));
	}
}

function instelling(naam) {
	var filter = instellingen.filter(function(obj) { return obj.naam == naam; });
	if(filter.length == 1) {
		return filter[0].waarde;
	} else {
		return filter;
	}
}

function generateUUID() {
	var d = new Date().getTime();
	
	if( window.performance && typeof window.performance.now === "function" )
	{
		d += performance.now();
	}
	
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
	{
		var r = (d + Math.random()*16)%16 | 0;
		d = Math.floor(d/16);
		return (c=='x' ? r : (r&0x3|0x8)).toString(16);
	});

return uuid;
}