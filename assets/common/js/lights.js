var timeoutId = 0;

function sortToggles() {
	if(!$(".remote_grid").hasClass("sort")) {
		$(".remote_grid").addClass("sort");
	    $(".remote_grid").sortable({
	        update: function(event, ui) {
	            var toggles = "";
	            $(".lampToggle").each(function() {
	                toggles = (toggles == "") ? $(this).data("id") : toggles + ","+$(this).data("id");
	            });
	            $.ajax({
	                url: '/backsite/lampen.php',
	                type: 'PUT',
	                data: {order:toggles},
	                success: function(result) {
//	                    console.log(result);
	                }
	            });
	        }
	        //$(".remote_grid").sortable( "enable" );
	    });
	} else {
		$(".remote_grid").removeClass("sort");
		$(".remote_grid").sortable( "disable" )
	}
}

$(document).on('mousedown touchstart', '.lampToggle', function() {
//    timeoutId = setTimeout(sortToggles, 1000);
}).on('mouseup mouseleave', function() {
//    clearTimeout(timeoutId);
});

$(document).on("mouseup", ".lampBewerken", function(e) {
	var id = $(e.target).closest(".lampToggle").data("id");
	$.get("/backsite/lampen.php", {id:id}, function(data) {
		var code = data.code.split(" ");
		$("#defaultModalLabel").text("Lamp bewerken");
		$("#defaultModal .modal-footer").prepend("<button type='button' id='lampFormDelete' class='btn btn-danger waves-effect pull-left' data-dismiss='modal'>Verwijderen</button>")
		$("#defaultModal").modal();
		$("#lampFormType").val("bewerken");
		$("#lampFormId").val(data.id);
		$("#lampFormNaam").val(data.naam);
		$("#lampFormCodeLetter").val(code[0]);
		$('#lampFormCodeLetter').selectpicker('refresh')
		$("#lampFormCodeNummer").val(code[1]);
		$('#lampFormCodeNummer').selectpicker('refresh')
		$("#lampFormKleur").val(data.kleur);
		$('#lampFormKleur').selectpicker('refresh')
	});

});

$('#defaultModal').on('hidden.bs.modal', function () {
	$("#lampFormType").val("nieuw");
	$("#lampFormId").val("0");
	$("#lampFormDelete").remove();
	$("#lampFormNaam").val("");
	$("#lampFormCodeLetter").val("A");
	$("#lampFormCodeNummer").val("1");
	$("#lampFormKleur").val("red");
})

$(document).on("mouseup", ".lampToggle", function(e) {
	if(!$(e.target).hasClass("lampBewerken") && !$(".remote_grid").hasClass("sort")) {
		
		$(".remote_grid").css("opacity", "0.5");
		$(".lampToggle").css("cursor", "default");

		if(!$(e.target).hasClass(".lampToggle")) {
			var id = $(e.target).closest(".lampToggle").data("id");
		} else {
			var id = $(e.target).data("id");
		}
		$.post("/backsite/actie.php", {id: id}, function(data) {
			if(data[0].success) {
				$(".remote_grid").css("opacity", "1");
				$(".lampToggle").css("cursor", "pointer");
				var icon = (data.status == 1) ? "lightbulb" : "lightbulb_outline";
				changeLampIcon(id, icon);
			}
		});
	}
});

$(document).on("click", "#btnAddLamp", function() {
	switch($("#lampFormType").val()) {
		case "nieuw":
			createLamp($("#lampFormNaam").val(), $("#lampFormConsoleType").val(), $("#lampFormCodeLetter").val() + " " + $("#lampFormCodeNummer").val(), $("#lampFormKleur").val());
		break;
		case "bewerken":
			updateLamp($("#lampFormId").val(), $("#lampFormNaam").val(), $("#lampFormConsoleType").val(), $("#lampFormCodeLetter").val() + " " + $("#lampFormCodeNummer").val(), $("#lampFormKleur").val());
		break;
	}
});

$(document).on("click", "#lampFormDelete", function(e) {
	var id = $("#lampFormId").val();
	$.ajax({
	    url: '/backsite/lampen.php',
	    type: 'DELETE',
	    data: {id:id},
	    success: function(result) {
//	        console.log(result);
	    }
	});
	$(".lampToggle[data-id='"+id+"']").remove();
})

function getLamps() {
	$.get("/backsite/lampen.php", function(data) {
		for(var i = 0; i < data.length; i++) {
			addLampToGrid(data[i].id, data[i].naam, data[i].kleur, data[i].status);
		}
	})
}

function addLampToGrid(id, naam, kleur, status) {
	var icon = (status == "1") ? "lightbulb" : "lightbulb_outline";
	$(".remote_grid").append(
	"<div class='lampToggle demo-color-box bg-"+kleur+" waves-effect waves-light' data-id='"+id+"'>"+
		"<i class='material-icons lampBewerken'>more_vert</i>"+
		"<i class='lamp-icon material-icons'>"+icon+"</i>"+
		"<div class='color-class-name'>"+naam+"</div>"+
	"</div>"
	)
}

function createLamp(naam, consoleType, code, kleur) {
	console.log(consoleType);
	$.post("/backsite/lampen.php", {naam:naam, type:consoleType, code:code, kleur:kleur}, function(data) {
		addLampToGrid(data.id, naam, kleur, data.status);
	})
}

function changeLampIcon(id, icon) {
	$(".lampToggle[data-id='"+id+"'] .lamp-icon").text(icon);
}

function updateLamp(id, naam, type, code, kleur) {
	$.ajax({
	    url: '/backsite/lampen.php',
	    type: 'PUT',
	    data: {id:id, naam: naam, code: code, kleur:kleur},
	    success: function(result) {
	        //console.log(result);
	    }
	});
	//Oude bg- class verwijderen
	$(".lampToggle[data-id='"+id+"']").removeClass(function (index, className) {
	    return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
	});
	$(".lampToggle[data-id='"+id+"']").addClass("bg-"+kleur);
	$(".lampToggle[data-id='"+id+"'] .color-class-name").text(naam);
}