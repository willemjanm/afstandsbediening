(function() {
	$("#logout").click(function() {
		$.post("/backsite/logout.php", function(data) {
			if(data.success) {
				window.location.href='/login.html';
			}
		});
	})

	$.post("/backsite/auth.php", function(data) {
		//check of de gebruiker op de inlogpagina is
		if(window.location.href.indexOf("login.html") > -1) {
			$("body").addClass("hidden");
			//gebruiker doorsturen naar indexpagina wanneer ingelogd
			if(data.loggedIn == true) {
				window.location.href='/index.html';
			} else {
				$("body").removeClass("hidden");
			}

			//formulier verzenden
			$(document).on("submit", "#sign_in", function(e) {
				e.preventDefault();
				//spinner weergeven en formulier uitschakelen
				$("#incorrect").slideUp(200);
				$(".preloader").removeClass("hidden").css("display", "none").slideDown(200);
				$("#sign_in input, #sign_in button").attr("disabled", "disabled");
				//wachtwoord checken
				var wachtwoord = $("input[name=password]").val();
				var rememberme = ($("input[name=rememberme]").prop('checked')) ? 1 : 0;
				$.post("/backsite/login.php", {wachtwoord: wachtwoord, rememberme: rememberme}, function(data) {
					if(data.success == true) {
						window.location.href='/index.html';
					} else {
						$(".preloader").slideUp(200);
						$("#sign_in input, #sign_in button").removeAttr("disabled");
						$("#incorrect").slideDown(200);
					}
				});
			});
		// gebruiker is niet op de inlogpagina
		} else {
			//gebruiker doorsturen naar inlogpagina wanneer niet ingelogd
			if(data.loggedIn == false) {
				window.location.href='/login.html';
			}
		}
	});
}());