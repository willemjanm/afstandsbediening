$(function() {
	$.get("backsite/notificaties.php", function(data) {
		for(var i = 0; i < data.length; i++) {
			$.notify({
				// options
				title: data[i].titel,
				message: data[i].tekst
			},{
				// settings
				delay: 0,
				onClose: function() { deleteNotificatie($(this).data("id")) },
				placement: {
					from: "bottom",
					align: "right"
				},
				template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert" role="alert" style="background: rgba(0, 0, 0, 0.7)" data-id="'+data[i].notificatieID+'">' +
					'<button type="button" aria-hidden="true" class="close" data-notify="dismiss" style="color: #ffffff; opacity: 1;">×</button>' +
					'<span data-notify="icon"></span> ' +
					'<span data-notify="title"><b>{1}</b></span><br> ' +
					'<span data-notify="message">{2}</span>' +
					'<div class="progress" data-notify="progressbar">' +
						'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
					'</div>' +
					'<a href="{3}" target="{4}" data-notify="url"></a>' +
				'</div>'
			});
		}
	})
});

function deleteNotificatie(id) {
	$.ajax({
	    url: '/backsite/notificaties.php',
	    type: 'DELETE',
	    data: {id:id},
	    success: function(result) {
//	        console.log(result);
	    }
	});
}