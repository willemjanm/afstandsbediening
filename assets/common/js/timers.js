$(document).on("click", ".timerBewerken", function(e) {
	var id = $(e.target).closest(".timerTegel").data("id");
	$.get("/backsite/timer.php", {id:id}, function(data) {
		$("#defaultModalLabel").text("Timer bewerken");
		$("#defaultModal .modal-footer").prepend("<button type='button' id='timerFormDelete' class='btn btn-danger waves-effect pull-left' data-dismiss='modal'>Verwijderen</button>")
		$("#defaultModal").modal();
		$("#timerFormType").val("bewerken");
		$("#timerFormId").val(data.timerID);
		$("#timerFormLamp").val(data.lampIDFK);
//		$("#timerFormLamp").selectpicker('refresh');
		$("#timerFormTijd").val(data.tijd);
		$("#timerFormActie").val(data.actie);
//		$("#timerFormActie").selectpicker('refresh');
		setActiveModal("formTimerMa", data.ma);
		setActiveModal("formTimerDi", data.di);
		setActiveModal("formTimerWo", data.wo);
		setActiveModal("formTimerDo", data.do);
		setActiveModal("formTimerVr", data.vr);
		setActiveModal("formTimerZa", data.za);
		setActiveModal("formTimerZo", data.zo);

		$(document).change(".formTimerday", function(e) {
			setActiveModal($(e.target).attr('id'), $(e.target).prop('checked'));
		})
	});
});

function setActiveModal(el, val) {
	if(val == 1 || val == true) {
		$("#"+el).prop('checked', true);
		$("label[for='"+el+"']").addClass("active");
	} else {
		$("#"+el).prop('checked', false);
		$("label[for='"+el+"']").removeClass("active");

	}
}
function setActiveGrid(el, val) {
	if(val == 1 || val == true) {
		el.addClass("active");
	} else {
		el.removeClass("active");

	}
}

$('#defaultModal').on('hidden.bs.modal', function () {
	$("#timerFormType").val("nieuw");
	$("#timerFormId").val("0");
	$("#timerFormDelete").remove();
	$("#timerFormLamp").val(1);
	$("#timerFormTijd").val("");
	$("#timerFormActie").val(1);
	setActiveModal("formTimerMa", false);
	setActiveModal("formTimerDi", false);
	setActiveModal("formTimerWo", false);
	setActiveModal("formTimerDo", false);
	setActiveModal("formTimerVr", false);
	setActiveModal("formTimerZa", false);
	setActiveModal("formTimerZo", false);
})

$(document).on("click", ".timerTegel", function(e) {
	if(!$(e.target).hasClass("timerBewerken")) {
		if(!$(e.target).hasClass(".timerTegel")) {
			var id = $(e.target).closest(".timerTegel").data("id");
		} else {
			var id = $(e.target).data("id");
		}
		console.log(id);
		$.ajax({
		    url: '/backsite/timer.php',
		    type: 'PUT',
		    data: {disable:id},
		    success: function(result) {
		    	var kleur = (result.actief == 1) ? result.kleur : "grey";
		        $(".timerTegel[data-id='"+id+"']").removeClass(function (index, className) {
				    return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
				});
				$(".timerTegel[data-id='"+id+"']").addClass("bg-"+kleur);
		    }
		});	
	}
});

$(document).on("click", "#btnAddTimer", function() {
	var days = {
		"ma": ($("#formTimerMa").prop('checked')) ? 1 : 0,
		"di": ($("#formTimerDi").prop('checked')) ? 1 : 0,
		"wo": ($("#formTimerWo").prop('checked')) ? 1 : 0,
		"do": ($("#formTimerDo").prop('checked')) ? 1 : 0,
		"vr": ($("#formTimerVr").prop('checked')) ? 1 : 0,
		"za": ($("#formTimerZa").prop('checked')) ? 1 : 0,
		"zo": ($("#formTimerZo").prop('checked')) ? 1 : 0
	}	

	switch($("#timerFormType").val()) {
		case "nieuw":
			createTimer($("#timerFormLamp").val(), $("#timerFormTijd").val(), $("#timerFormActie").val(), days);
		break;
		case "bewerken":
			updateTimer($("#timerFormId").val(), $("#timerFormLamp").val(), $("#timerFormTijd").val(), $("#timerFormActie").val(), days);
		break;
	}
});

$(document).on("click", "#timerFormDelete", function(e) {
	var id = $("#timerFormId").val();
	$.ajax({
	    url: '/backsite/timer.php',
	    type: 'DELETE',
	    data: {id:id},
	    success: function(result) {
	    	$(".timerTegel[data-id='"+id+"']").remove();
	    }
	});
})

function getTimers() {
	$.get("/backsite/timer.php", function(data) {
		for(var i = 0; i < data.length; i++) {
			var days = {"ma": data[i].ma, "di": data[i].di, "wo": data[i].wo, "do": data[i].do, "vr": data[i].vr, "za": data[i].za, "zo": data[i].zo}
			addTimerToGrid(data[i].timerID, data[i].lampIDFK, data[i].naam, data[i].tijd, data[i].actie, data[i].kleur, data[i].actief, days);
		}
	})
	$.get("/backsite/lampen.php", function(data) {
		for(var i = 0; i < data.length; i++) {
			$("#timerFormLamp").append("<option value='"+data[i].id+"'>"+data[i].naam+"</option>");
		}
	})
}

function addTimerToGrid(timerID, lampID, naam, tijd, actie, kleur, actief, days) {
	actie = (actie == 1) ? "aan" : "uit";
	tijd = tijd.split(":");
	tijd = tijd[0] + ":" + tijd[1];
	kleur = (actief == 1) ? kleur : "grey";
	for(day in days) {
		days[day] = (days[day] == 1) ? "active" : "";
	}
	$(".timer_grid").append(
	"<div class='timerTegel demo-color-box bg-"+kleur+" waves-effect waves-light' data-id='"+timerID+"'>"+
		"<i class='material-icons timerBewerken'>more_vert</i>"+
		"<div class='lampNaam'>"+naam+"</div>"+
		"<div class='timerTijd'>"+tijd+"</div>"+
		"<div class='timerActie'>Actie: "+actie+"</div>"+
		"<ul class='days'>"+
			"<li class='"+days.ma+"' data-day='ma'>Ma</li>"+
			"<li class='"+days.di+"' data-day='di'>Di</li>"+
			"<li class='"+days.wo+"' data-day='wo'>Wo</li>"+
			"<li class='"+days.do+"' data-day='do'>Do</li>"+
			"<li class='"+days.vr+"' data-day='vr'>Vr</li>"+
			"<li class='"+days.za+"' data-day='za'>Za</li>"+
			"<li class='"+days.zo+"' data-day='zo'>Zo</li>"+
		"</ul>"+
	"</div>"
	)
}

function createTimer(lamp, tijd, actie, days) {
	var volgorde = $(".timer_grid .timerTegel").length + 1;
	$.post("/backsite/timer.php", {lamp:lamp, tijd:tijd, actie:actie, volgorde:volgorde, "ma":days.ma, "di":days.di, "wo":days.wo, "do":days.do, "vr":days.vr, "za":days.za, "zo":days.zo}, function(data) {
		var days = {"ma": data.ma, "di": data.di, "wo": data.wo, "do": data.do, "vr": data.vr, "za": data.za, "zo": data.zo}
		addTimerToGrid(data.timerID, data.lampIDFK, data.naam, data.tijd, data.actie, data.kleur, data.actief, days);
	})
}

function updateTimer(id, lamp, tijd, actie, days) {
	$.ajax({
	    url: '/backsite/timer.php',
	    type: 'PUT',
	    data: {id:id, lamp: lamp, tijd: tijd, actie:actie, "ma":days.ma, "di":days.di, "wo":days.wo, "do":days.do, "vr":days.vr, "za":days.za, "zo":days.zo},
	    success: function(result) {
	    	var kleur = (result.actief == 1) ? result.kleur : "grey";
	    	var actie = (result.actie == 1) ? "aan" : "uit";
			var tijd = result.tijd.split(":");
				tijd = tijd[0] + ":" + tijd[1];
			//Oude bg- class verwijderen
			$(".timerTegel[data-id='"+id+"']").removeClass(function (index, className) {
			    return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
			});
			$(".timerTegel[data-id='"+id+"']").addClass("bg-"+kleur);
			$(".timerTegel[data-id='"+id+"'] .lampNaam").text(result.naam);
			$(".timerTegel[data-id='"+id+"'] .timerTijd").text(tijd);
			$(".timerTegel[data-id='"+id+"'] .timerActie").text("Actie: " + actie);
			setActiveGrid($(".timerTegel[data-id='"+id+"'] .days li[data-day='ma']"), result.ma);
			setActiveGrid($(".timerTegel[data-id='"+id+"'] .days li[data-day='di']"), result.di);
			setActiveGrid($(".timerTegel[data-id='"+id+"'] .days li[data-day='wo']"), result.wo);
			setActiveGrid($(".timerTegel[data-id='"+id+"'] .days li[data-day='do']"), result.do);
			setActiveGrid($(".timerTegel[data-id='"+id+"'] .days li[data-day='vr']"), result.vr);
			setActiveGrid($(".timerTegel[data-id='"+id+"'] .days li[data-day='za']"), result.za);
			setActiveGrid($(".timerTegel[data-id='"+id+"'] .days li[data-day='zo']"), result.zo);
	    }
	});
}