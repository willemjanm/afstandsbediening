<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('functions.php');

if(checkLoggedIn() == true) {

	require_once("lib/MySQLi/MysqliDb.php");

	//Database connectie
	$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

	$_POST = array_merge($_POST, $_GET);

	if($_POST['id'] === "all") {
		//Lampstatus en code verkrijgen
		$lampen = $db->get('lamp');
	} else {
		//Lampstatus en code verkrijgen
		$db->where ("id", $_POST['id']);
		$lampen = [$db->getOne('lamp')];
	}

	$json = [];

	foreach($lampen AS $lamp) {

		//Actie instellen
		if(isset($_POST['actie'])) {
			$action = $_POST['actie'];
		} else {
			$action = ($lamp['status'] == 0) ? "on" : "off";
		}

		//Actie uitvoeren
		exec("cd /home/pi/wiringPi/examples/lights && ./".$lamp['type']." ".$lamp['code']." ".$action);
		$newStatus = ($action == "on") ? 1 : 0;
		$json[] = Array("success" => true, "status" => $newStatus);

		echo $lamp['id'];

		//Nieuwe status instellen
		$data = Array('status' => $newStatus);
		$db->where("id", $lamp['id']);
		$db->update ('lamp', $data);
	}
} else {
	$json['success'] = false;
	$json['error'] = 'Niet ingelogd!';
}

header('Content-Type: application/json');
echo json_encode($json);
?>