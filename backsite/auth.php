<?php

require_once('functions.php');

$auth['loggedIn'] = checkLoggedIn();

header('Content-Type: application/json');
echo json_encode($auth);