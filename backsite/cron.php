<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$tijd = date('H:i:00');
$days = ["ma", "di", "wo", "do", "vr", "za", "zo"];
$d = date('N') - 1;
//echo date("H:i:s");

require_once("lib/MySQLi/MysqliDb.php");

//Database connectie
$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

//Lampstatus en code verkrijgen
$db->join("lamp l", "l.id=t.lampIDFK", "LEFT");
$db->where ($days[$d], 1);
$db->where ("tijd", $tijd);
$db->where ("actief", 1);

$timers = $db->get('timer t', null, 't.*, l.type, l.code');

for($i = 0; $i < count($timers); $i++) {
	$actie = ($timers[$i]['actie'] == 1) ? "on" : "off";
	exec("cd /home/pi/wiringPi/examples/lights && ./".$timers[$i]['type']." ".$timers[$i]['code']." ".$actie);
	
	$data = Array('status' => $timers[$i]['actie']);
	$db->where("id", $timers[$i]['lampIDFK']);
	$db->update ('lamp', $data);
}

?>