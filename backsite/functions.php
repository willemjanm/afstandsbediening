<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

session_start();
function checkLoggedIn() {

	require_once("lib/MySQLi/MysqliDb.php");

	$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

	// DB cleanup!
	$db->where('vervalDatum', time(), "<");
	$db->delete('cookie');

	if(isset($_COOKIE['auth']) || isset($_SESSION['auth'])) {
		if(isset($_SESSION['auth'])) {
			$loggedIn = true; // geen gergelijking nodig, want sessies
		} else {
			$localCookie = $_COOKIE['auth'];
			$db->where('hash', $localCookie);
			$cookie = $db->getOne('cookie');
			$loggedIn = ($localCookie == $cookie['hash']) ? true : false;
		}
	} else {
		$loggedIn = false; //geen cookies of sessions
	}

	$db->where('naam', 'ipaddress');
	$ipaddress = $db->getOne('instelling');

	$local = (substr($_SERVER['REMOTE_ADDR'], 0, 7) == "192.168" || $_SERVER['REMOTE_ADDR'] == get_server_ip()) ? true : false;
	// $local = false;

	if(isset($_GET['APIkey'])) {
		$db->where('naam', 'APIkey');
		$dbKey = $db->getOne('instelling');
		$api = ($dbKey['waarde'] == $_GET['APIkey']) ? true : false;
	} else {
		$api = false;
	}

	if($local == true || $loggedIn == true || $api == true) {
		return true;
	} else {
		return false;
	}


//	return Array("local" => $local, "loggedIn" => $loggedIn);
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function get_server_ip() {
	$externalContent = file_get_contents('http://checkip.dyndns.com/');
	preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
	return $m[1];
}

?>