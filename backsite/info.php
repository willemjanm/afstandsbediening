<?php
require_once("lib/MySQLi/MysqliDb.php");
require_once("functions.php");

//Database connectie
$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

$db->where('hash', $_COOKIE["auth"]);
$cookie = $db->getOne('cookie');
$vervalDatum = ($db->count > 0) ? date("d-m-Y H:i", $cookie['vervalDatum']) : "<i>Leeg</i>";
$sessie = (isset($_SESSION['auth'])) ? $_SESSION['auth'] : "<i>Leeg</i>";

?>
<html>
<head>
	<title>Afstandsbediening instellingen</title>
</head>
<body>

<table cellpadding='5'>
	<tr>
		<td>Tijd</td>
		<td><?php echo date("H:i:s"); ?></td>
	</tr>
	<tr>
        <td>Server IP</td>
        <td><?php echo get_server_ip(); ?></td>
    </tr>
		<td>Authenticated</td>
		<td><?php if(checkLoggedIn()) { echo "true"; } else { echo "false"; } ?></td>
    </tr>
        <td>Client IP</td>
        <td><?php echo get_client_ip(); ?></td>
    </tr>
    </tr>
        <td>Cookie vervaldatum</td>
        <td><?php echo $vervalDatum; ?></td>
    </tr>
    </tr>
        <td>Sessie</td>
        <td><?php echo $sessie; ?></td>
    </tr>
</table>

</body>
</html>