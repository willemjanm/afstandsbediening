<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("functions.php");
if(checkLoggedIn() == true) {

	require_once("lib/MySQLi/MysqliDb.php");

	//Database connectie
	$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

	//lampen verkrijgen
	if($_SERVER['REQUEST_METHOD'] == "GET") {
		if(isset($_GET['naam'])) {
			$db->where("naam", $_GET['naam']);
			$instellingen = $db->getOne('instelling');
		} else {
			$instellingen = $db->get('instelling');
		}
		header('Content-Type: application/json');
		echo json_encode($instellingen);
	}

	if($_SERVER['REQUEST_METHOD'] == "POST") {

	}

	if($_SERVER['REQUEST_METHOD'] == "PUT") {
		parse_str(file_get_contents('php://input'), $_PUT);
		$json['success'] = true;
		foreach($_PUT as $key => $val) {
			if($key == "wachtwoord") {
				if($val != "") {
					if($_PUT['wachtwoord'] == $_PUT['wachtwoord2']) {
						$val = password_hash($val, PASSWORD_DEFAULT, ['cost' => 12]); 
						$json['success'] = ($json['success']) ? true : false;
					} else {
						$json['success'] = false;
						$json['error'] = "De wachtwoorden komen niet overeen!";
					}
				} else {
					$db->where('naam', 'wachtwoord');
					$val = $db->getOne('instelling')['waarde'];
				}
			}
			$data = Array('waarde' => $val);
			$db->where('naam', $key);
			$db->update('instelling', $data);
			$json['success'] = ($json['success']) ? true : false;
		}
		header('Content-Type: application/json');
		echo json_encode($json);
	}

	if($_SERVER['REQUEST_METHOD'] == "DELETE") {
		parse_str(file_get_contents('php://input'), $_DELETE);

	}
}
?>