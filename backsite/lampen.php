<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require_once("functions.php");
if(checkLoggedIn() == true) {

	require_once("lib/MySQLi/MysqliDb.php");

	//Database connectie
	$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

	//lampen verkrijgen
	if($_SERVER['REQUEST_METHOD'] == "GET") {
		if(isset($_GET['id'])) {
			$db->where("id", $_GET['id']);
			$lamps = $db->getOne('lamp');
		} else {
			$db->orderBy("volgorde","asc");
			$lamps = $db->get('lamp');
		}
		header('Content-Type: application/json');
		echo json_encode($lamps);
	}

	if($_SERVER['REQUEST_METHOD'] == "POST") {
		$data = Array (
			"naam" => $_POST['naam'],
			"type" => $_POST['type'],
			"code" => $_POST['code'],
			"kleur" => $_POST['kleur']
		);
		
		$id = $db->insert ('lamp', $data);
		$json = Array("id" => $id);
		header('Content-Type: application/json');
		echo json_encode($json);
	}

	if($_SERVER['REQUEST_METHOD'] == "PUT") {
		parse_str(file_get_contents('php://input'), $_PUT);
		if(isset($_PUT['id'])) {
			$db->where ("id", $_PUT['id']);
			$data = Array (
				"naam" => $_PUT['naam'],
				"code" => $_PUT['code'],
				"kleur" => $_PUT['kleur']
			);
			$db->where("id", $_PUT['id']);
			$db->update ('lamp', $data);
		}
		if(isset($_PUT['order'])) {
			$order = explode(",", $_PUT['order']);
			for($i = 0; $i < count($order); $i++) {
				$db->where ("id", $order[$i]);
				$data = Array("volgorde" => ($i + 1));
				$db->update ('lamp', $data);
			}
		}
	}

	if($_SERVER['REQUEST_METHOD'] == "DELETE") {
		parse_str(file_get_contents('php://input'), $_DELETE);
		$db->where ("id", $_DELETE['id']);
		$db->delete ('lamp');
	}
}
?>