<?php
session_start();
if($_SERVER['REQUEST_METHOD'] == "POST") {
	require_once("lib/MySQLi/MysqliDb.php");

	//Database connectie
	$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

	$db->where("naam", "wachtwoord");
	$wachtwoord = $db->getOne("instelling");

	$checked = password_verify($_POST['wachtwoord'], $wachtwoord['waarde']);

	//Als wachtwoord correct is
	if ($checked) {
		//random has genereren welke later wordt gebruikt om te kijken of de cookie de juiste waarde heeft
		$hash = bin2hex(random_bytes(20));

		if($_POST['rememberme'] == 0) {
			//sessie zetten als gebruiker niet ingelogd wil blijven
			$_SESSION['auth'] = $hash;
		} else {
			//cookie zetten voor 4 maanden als de gebruiker ingelogd wil blijven
			//zelfde waarde in de database opslaan om de cookie daarme te vergelijken

			$vervalDatum = time() + (86400 * 120);

			$data = Array (
				"hash" => $hash,
				"vervalDatum" => $vervalDatum
			);

			$db->insert ('cookie', $data);
			setcookie("auth", $hash, $vervalDatum, "/");
		}
		$json['success'] = true;
	} else {
		setcookie("auth", $hash, time() - 3600, "/");
		$json['success'] = false;
	}

	header('Content-Type: application/json');
	echo json_encode($json);
}