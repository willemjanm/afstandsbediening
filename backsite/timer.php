<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once("functions.php");
if(checkLoggedIn() == true) {
	require_once("lib/MySQLi/MysqliDb.php");

	//Database connectie
	$db = new MysqliDb ('localhost', 'root', 'oh553jUv', 'lampen');

	//timers verkrijgen
	if($_SERVER['REQUEST_METHOD'] == "GET") {
		if(isset($_GET['id'])) {
			$db->join("lamp l", "l.id=t.lampIDFK", "LEFT");
			$db->where("timerID", $_GET['id']);
			$timers = $db->getOne('timer t');
		} else {
			$db->join("lamp l", "l.id=t.lampIDFK", "LEFT");
			$db->orderBy("volgorde","asc");
			$timers = $db->get('timer t', null, 't.*, l.naam, l.kleur');
		}
		header('Content-Type: application/json');
		echo json_encode($timers);
	}

	if($_SERVER['REQUEST_METHOD'] == "POST") {
		$actie = ($_POST['actie'] == "1") ? true : false;
		$data = Array (
			"lampIDFK" => $_POST['lamp'],
			"tijd" => $_POST['tijd'],
			"actie" => $actie,
			"volgorde" => $_POST['volgorde'],
			"actief" => 1,
			"ma" => $_POST['ma'],
			"di" => $_POST['di'],
			"wo" => $_POST['wo'],
			"do" => $_POST['do'],
			"vr" => $_POST['vr'],
			"za" => $_POST['za'],
			"zo" => $_POST['zo'],
		);
		
		$id = $db->insert ('timer', $data);
		if($id) {
			$db->join("lamp l", "l.id=t.lampIDFK", "LEFT");
			$db->where("timerID", $id);
			$timer = $db->getOne('timer t', null, 't.*, l.naam, l.kleur');
			header('Content-Type: application/json');
			echo json_encode($timer);
		} else {
			echo $db->getLastError();
		}
	}

	if($_SERVER['REQUEST_METHOD'] == "PUT") {
		parse_str(file_get_contents('php://input'), $_PUT);
		if(isset($_PUT['id'])) {
			$actie = ($_PUT['actie'] == "1") ? true : false;
			$db->where ("timerID", $_PUT['id']);
			$data = Array (
				"lampIDFK" => $_PUT['lamp'],
				"tijd" => $_PUT['tijd'],
				"actie" => $actie,
				"ma" => $_PUT['ma'],
				"di" => $_PUT['di'],
				"wo" => $_PUT['wo'],
				"do" => $_PUT['do'],
				"vr" => $_PUT['vr'],
				"za" => $_PUT['za'],
				"zo" => $_PUT['zo'],
			);
			$db->update ('timer', $data);

			//where voor de volgende select query
			$db->where ("timerID", $_PUT['id']);
		}
		if(isset($_PUT['disable'])) {
			$db->where ("timerID", $_PUT['disable']);
			$data = Array("actief" => $db->not());
			$db->update ('timer', $data);

			//where voor de volgende select query
			$db->where ("timerID", $_PUT['disable']);
		}
		$db->join("lamp l", "l.id=t.lampIDFK", "LEFT");
		$result = $db->getOne('timer t', null, 't.*, l.naam, l.kleur');
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	if($_SERVER['REQUEST_METHOD'] == "DELETE") {
		parse_str(file_get_contents('php://input'), $_DELETE);
		$db->where ("timerID", $_DELETE['id']);
		$db->delete ('timer');
	}
}
?>